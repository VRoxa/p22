package vrr.dam.com.pt22.extraActivities;

import android.content.Context;
import android.content.ContextWrapper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import vrr.dam.com.pt22.R;

public class ListPhotosActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    PhotoAdapter adapter;
    ArrayList<File> photos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_photos);

        recyclerView = findViewById(R.id.listview);

        ContextWrapper cw = new ContextWrapper(this);
        String FILE_PATH = cw.getDir("images", Context.MODE_PRIVATE).getAbsolutePath();
        File[] files = new File(FILE_PATH).listFiles();
        if (files != null)
            photos = new ArrayList<>(Arrays.asList(new File(FILE_PATH).listFiles()));

        adapter = new PhotoAdapter(recyclerView, this, photos);
        recyclerView.setAdapter(adapter);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

    }
}
