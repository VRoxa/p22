package vrr.dam.com.pt22.extraActivities;

import android.graphics.BitmapFactory;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import vrr.dam.com.pt22.R;

public class FullSizedImage extends AppCompatActivity {
    ConstraintLayout layout;
    ConstraintLayout.LayoutParams PARAMS = new ConstraintLayout.LayoutParams
            (
                ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT
            );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_sized_image);

        layout = findViewById(R.id.layout);
        String path = getIntent().getStringExtra("Path");

        ImageView img = new ImageView(this);
        try {
            img.setImageBitmap(BitmapFactory.decodeStream(
                    new FileInputStream(
                            new File(path)
                    )
            ));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        img.setLayoutParams(PARAMS);
        layout.addView(img);
    }
}
