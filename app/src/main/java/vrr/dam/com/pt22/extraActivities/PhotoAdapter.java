package vrr.dam.com.pt22.extraActivities;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import vrr.dam.com.pt22.R;

public class PhotoAdapter
        extends RecyclerView.Adapter<PhotoAdapter.MyViewHolder>{
    private RecyclerView recyclerView;
    private Context context;
    private ArrayList<File> photos;

    public PhotoAdapter(RecyclerView recyclerView, Context context, ArrayList<File> photos) {
        this.recyclerView = recyclerView;
        this.context = context;
        this.photos = photos;
    }

    @NonNull
    @Override
    public PhotoAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_layout, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoAdapter.MyViewHolder myViewHolder, int i) {
        File f = photos.get(i);
        myViewHolder.photo_name.setText(f.getName());
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView photo_name;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            photo_name = itemView.findViewById(R.id.photo_name);

            itemView.setOnClickListener((v) -> {
                Intent intent = new Intent(context, FullSizedImage.class);

                ContextWrapper cw = new ContextWrapper(context);
                String FILE_PATH = cw.getDir("images", Context.MODE_PRIVATE).getAbsolutePath();
                //Log.d("PATH", FILE_PATH + photo_name.getText().toString());
                intent.putExtra("Path", FILE_PATH + File.separator + photo_name.getText().toString());
                context.startActivity(intent);
            });
        }
    }
}
