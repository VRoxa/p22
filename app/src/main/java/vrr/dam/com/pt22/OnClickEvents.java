package vrr.dam.com.pt22;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class OnClickEvents {
    static boolean left = false;

    /**
     * Agrega el texto que se encuentra en el EditText al ScrollView
     * @param context Contexto en el que se encuentra
     * @param text Contenedor del EditText
     * @param params Propiedades de la nueva View
     * @param layout Contenedor donde agregar el nuevo TextView
     */
    static void send (Context context,
                      EditText text,
                      LinearLayout.LayoutParams params,
                      LinearLayout layout) {
        String textToSend = text.getText().toString();

        // Sólo postea si hay texto
        if (! textToSend.isEmpty()) {
            params = setGravityToParams(params);

            TextView msg = new TextView(context);
            msg.setText(textToSend);

            // Carga el background left y agrega margin a la derecha
            if (left) {
                msg.setBackground(context.getDrawable(R.drawable.chat_left));
                // left top right bottom
                params.setMargins(
                        msg.getPaddingLeft(),
                        msg.getPaddingTop(),
                        400,
                        msg.getPaddingBottom() - 100
                );
            }
            // Carga el background right y agrega margin a la izq.
            else {
                msg.setBackground(context.getDrawable(R.drawable.chat_right));
                params.setMargins(
                        400,
                        msg.getPaddingTop(),
                        msg.getPaddingRight(),
                        msg.getPaddingBottom() - 100
                );
            }

            // Se agregan las propiedades a la View
            // Se añade la View al Scroll
            // Se reseta el EditText
            msg.setLayoutParams(params);
            layout.addView(msg);
            text.setText("");
            text.setHint(context.getString(R.string.hint));

            left = !left;
        }
    }

    /**
     * Agrega un ImageView redimensionado al Scroll
     * @param activity Contexto en el que se encuentra
     * @param data Contiene los recursos necesarios
     * @param params Propiedades de la nueva View
     * @param layout Contenedor donde agregar el nuevo ImageView
     */
    static void postImage (Activity activity,
                           @Nullable Intent data,
                           LinearLayout.LayoutParams params,
                           LinearLayout layout) {
        //Bitmap img = (Bitmap) data.getExtras().get("data");
        if (data != null) {
            params = setGravityToParams(params);

            Bitmap b = (Bitmap) data.getExtras().get("data");

            String PATH = OnClickEvents.saveToInternalStorage(activity, b);
            ImageViewWithExtras img = new ImageViewWithExtras(activity, PATH);
            // Se escala el Bitmap a 720px
            b = downscaleToMaxAllowedDimension(b, 720);

            img.setImageBitmap(b);

            img.setLayoutParams(params);
            img.setBackground(left ?
                    activity.getDrawable(R.drawable.chat_left) :
                    activity.getDrawable(R.drawable.chat_right));
            layout.addView(img);

            left = !left;
        }
    }

    /**
     * Añade un ImageView des de Resources al Scroll
     * @param context Contexto en el que se encuentra
     * @param params Propiedades de la nueva View
     * @param layout Contenedor donde agregar el nuevo ImageView
     */
    static void sendEmoji(Context context,
                          LinearLayout.LayoutParams params,
                          LinearLayout layout) {
        params = setGravityToParams(params);

        // Se rescata el .png de los Resources
        // Se escala el Bitmap a 128px
        ImageView img = new ImageView(context);
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.fist);
        bitmap = downscaleToMaxAllowedDimension(bitmap, 128);
        img.setImageBitmap(bitmap);

        // Se le da padding bottom a la imagen
        img.setLayoutParams(params);
        img.setPadding(
                img.getLeft(),
                img.getTop(),
                img.getRight(),
                100
        );
        layout.addView(img);

        left = !left;
    }

    /**
     * @author Geobits (StackOverflow)
     */
    private static Bitmap downscaleToMaxAllowedDimension(Bitmap bitmap, int maxAllowedDimension) {
        int outWidth;
        int outHeight;
        int inWidth = bitmap.getWidth();
        int inHeight = bitmap.getHeight();
        if(inWidth > inHeight){
            outWidth = maxAllowedDimension;
            outHeight = (inHeight * maxAllowedDimension) / inWidth;
        } else {
            outHeight = maxAllowedDimension;
            outWidth = (inWidth * maxAllowedDimension) / inHeight;
        }

        return Bitmap.createScaledBitmap(bitmap, outWidth, outHeight, false);
    }

    /**
     * Se le asigna el atributo Gravity en función del controlador left
     * @param params la LayoutParams a modificar
     * @return La nueva LayoutParams
     */
    private static LinearLayout.LayoutParams setGravityToParams (LinearLayout.LayoutParams params) {
        params.gravity = left ? Gravity.START : Gravity.END;
        return params;
    }

    private static String saveToInternalStorage (Context context, Bitmap bitmapImage){
        ContextWrapper cw = new ContextWrapper(context);
        File directory = cw.getDir("images", Context.MODE_PRIVATE);
        File path= new File(directory, LocalDateTime.now() + ".png");

        try (FileOutputStream fos = new FileOutputStream(path)) {
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return path.getAbsolutePath();
    }
}
