package vrr.dam.com.pt22;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;

import vrr.dam.com.pt22.extraActivities.ListPhotosActivity;

public class MainActivity extends AppCompatActivity {

    ScrollView scrollView;
    LinearLayout layout;
    LinearLayout.LayoutParams params;
    EditText text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        scrollView = findViewById(R.id.scrollview);
        layout = findViewById(R.id.scroll);
        text = findViewById(R.id.inputText);

        Log.d("MAIN ACTIVITY", "Created");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.photos:
                startActivity(new Intent(this, ListPhotosActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void onClick (View view) {
        params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        if (view.getId() == R.id.send_button)
            OnClickEvents.send(this, text, params, layout);

        else if (view.getId() == R.id.cameraBut)
            startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE), 0);

        else if (view.getId() == R.id.emojiBut)
            OnClickEvents.sendEmoji(this, params, layout);

        scrollView.post(() -> scrollView.fullScroll(View.FOCUS_DOWN));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK)
            OnClickEvents.postImage(this, data, params, layout);

        // Hay que hacer el scroll hacia abajo
        // porque onActivityResult() se llama de forma asíncrona con onClick()
        // Se postea la foto después de hacer el scroll.
        scrollView.post(() -> scrollView.fullScroll(View.FOCUS_DOWN));
    }
}
