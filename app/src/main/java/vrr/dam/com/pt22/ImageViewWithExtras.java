package vrr.dam.com.pt22;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import vrr.dam.com.pt22.extraActivities.FullSizedImage;

public class ImageViewWithExtras extends android.support.v7.widget.AppCompatImageView{
    String path;

    public ImageViewWithExtras(Context context, String path) {
        super(context);
        this.path = path;

        setOnClickListener((view) -> {
            // TODO Post Fullscreen Mode photo from PATH
            Log.d("POSTED IMAGE", path);


            Intent intent = new Intent(context, FullSizedImage.class);
            intent.putExtra("Path", path);
            context.startActivity(intent);
        });
    }
}
